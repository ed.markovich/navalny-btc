

```{r setup, include=FALSE}
knitr::opts_chunk$set(warning = FALSE,
message = FALSE,
cache = TRUE)
```

```{r}

library(ggplot2)
library(lubridate)
library(plotly)

source("Navalny Data Loader.R")
all_txin_txout <- load_all_transactions()
```

In the Address Exploration notebook I noticed some vanity-looking addresses. This notebook will explore whether
we can detect this statistically...

```{r}
addr <- as.character(unique(sources$adr))
```


Let's TRY to create a distribution of characters by string position...



```{r fig.width=12, fig.height=20}

char_distributions <- data.frame()

for (position in 1:34) {
  x<-substr(addr,position,position) %>% table() %>% data.frame(pos=position)
  char_distributions <- rbind(char_distributions, x)
}

colnames(char_distributions) <- c("Char", "Freq", "Pos")

char_distributions %>% filter(Pos<3) %>%
  ggplot() + geom_col(aes(x=Char, y=Freq)) + facet_wrap(~Pos, ncol=2)

char_distributions %>% filter(Pos>=3 & Pos <=33) %>%
  ggplot() + geom_col(aes(x=Char, y=Freq)) + facet_wrap(~Pos, ncol=2)

char_distributions %>% filter(Pos>=3 & Pos <=33) %>%
  ggplot() + geom_col(aes(x=Char, y=Freq)) 

```


So the 1st character is overwhelmingly 1 but occasionally 3, which is consistent with the protocol. It's not at all clear why the 2nd character is rather limited in range, perhaps it's some side effect of the numeric range. The other characters are evenly distributed more or less in the alpha-numeric space as evident by the graph faceted by position and the total aggregation.


```{r}
x<-substr(addr, 2, 100)
grep("putin",x, ignore.case=TRUE, value=TRUE) 
grep("durov",x, ignore.case=TRUE, value=TRUE)
grep("loshagin",x, ignore.case=TRUE, value=TRUE)
grep("RU",x, ignore.case=FALSE, value=TRUE)

#some possibilities:
# 7UPd8RUsQ2h1otMnQ2zYTa8iJzinHVuZd
# CT8RUN4FjSkMHEB2NCAC9V6vL2XjDXb2n
# PutinVorpJX1JrMKukXDyG1VaMFBGWgRU
# PutinHuj28G1Bc1WvFb9WxEKAKUDPUNEg
# DURoVpRb6ZyGHYdh1DFjLvje5AFGdYKgd
# LoshagintUV9vze3o2kzF7Hyv9kq2ivo2
```


I guess finding vanity addresses is not as easy as I would hope...